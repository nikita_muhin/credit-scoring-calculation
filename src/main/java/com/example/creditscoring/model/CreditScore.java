package com.example.creditscoring.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreditScore {

    @Id
    private Long id;

    private Double scoreByAge = 0.0;

    private Double scoreBySex = 0.0;

    private Double scoreByCivilState = 0.0;

    private Double scoreByFamilyMembersNumber = 0.0;

    private Double scoreByHomePlaceFrom = 0.0;

    private Double scoreByEducationLevel = 0.0;

    private Double scoreByContactInformation = 0.0;

    private Double scoreBySalaryNet = 0.0;

    private Double scoreByIncomeOtherSources = 0.0;

    private Double scoreByActivitySector = 0.0;

    private Double scoreByJobActivityFrom = 0.0;

    private Double scoreByDelaysDay = 0.0;

    private Double scoreByDebtAmountMDL = 0.0;

}
